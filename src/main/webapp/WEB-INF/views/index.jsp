<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>

<c:url value="/resources" var="resources" />
<c:url value="/userProfile" var="userProfile" />

<!DOCTYPE html>
<!--[if lt IE 9]><html class="lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--><html lang="en"><!--<![endif]-->
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Kudos</title>
	
	<!--[if lt IE 9]>
	  <script src="${resources}/js/html5shiv.min.js"></script>
	<![endif]-->
	
	<link href="${resources}/css/uswds.min.css" rel="stylesheet" />
	<link href="${resources}/css/kudos.css" rel="stylesheet" />
</head>

<body>
	<div class="main-content usa-center" id="main-content">
		<div class="usa-content">
			<header>
				<h1>Kudos</h1>
			</header>
			
			<button id="beginButton">Begin...</button>
		</div>
	</div>

	<script src="${resources}/js/uswds.min.js"></script>
	<script src="${resources}/js/jquery-3.1.0.min.js"></script>
	
	<script>
	$(document).ready(function() {
		$("#beginButton").click(function() {
			window.location.href = "${userProfile}"
		});
	});
	</script>

</body>
</html>
