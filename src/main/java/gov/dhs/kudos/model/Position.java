package gov.dhs.kudos.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Shane Hogan on 8/25/2016.
 */
@Entity
@Table(name = "POSITION")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Position implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "TYPE", unique = true, nullable = false)
    private PositionType type;

    public long getId() {
        return id;
    }

    public void setId(final long id) {
        this.id = id;
    }

    public PositionType getType() {
        return type;
    }

    public void setType(final PositionType type) {
        this.type = type;
    }

    public Position() {
    }

    public Position(PositionType type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Position position = (Position) o;

        return type.equals(position.type);

    }

    @Override
    public int hashCode() {
        return type.hashCode();
    }

    @Override
    public String toString() {
        return "Position{" +
                "id=" + id +
                ", type=" + type +
                '}';
    }
}
