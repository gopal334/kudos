package gov.dhs.kudos.model;

/**
 * Created by Shane Hogan on 8/25/2016.
 */
public enum PositionType {
    MANAGER("MANAGER"), ANALYST("ANALYST"), SPECIALIST("SPECIALIST"), SECRETARY("SECRETARY");

    private String positionType;

    PositionType(String positionType) {
        this.positionType = positionType;
    }

    public String getPositionType() {
        return positionType;
    }
}
